﻿/*
* Copyright Digital Mate
* Author: YoNeXia <yohann.neraud@gmail.com>
* Projects: https://gitlab.com/YoNeXia or https://github.com/YoNeXia
* Git: https://github.com/alicework/digitalmate-show-room
*/

using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

namespace VR_Showroom.Scripts.Lobby
{
    public class RoomListEntry : MonoBehaviour
    {
        #region Public Fields

        public Text RoomNameText;
        public Text RoomPlayersText;
        public Button JoinRoomButton;

        #endregion


        #region Private Serializable Fields

        #endregion


        #region Private Fields

        private string roomName;

        #endregion


        #region MonoBehaviour CallBacks

        public void Start()
        {
            JoinRoomButton.onClick.AddListener(() =>
            {
                if (PhotonNetwork.InLobby)
                {
                    PhotonNetwork.LeaveLobby();
                }

                PhotonNetwork.JoinRoom(roomName);
            });
        }

        #endregion


        #region MonoBehaviourPun CallBacks

        #endregion


        #region Public Methods

        // Initialize room name for multiplayer
        public void Initialize(string name, byte currentPlayers, byte maxPlayers)
        {
            roomName = name;

            RoomNameText.text = name;
            RoomPlayersText.text = currentPlayers + " / " + maxPlayers;
        }

        #endregion


        #region Private Methods

        #endregion
    }
}