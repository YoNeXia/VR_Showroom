﻿/*
* Copyright Digital Mate
* Author: YoNeXia <yohann.neraud@gmail.com>
* Projects: https://gitlab.com/YoNeXia or https://github.com/YoNeXia
* Git: https://github.com/alicework/digitalmate-show-room
*/

using UnityEngine;
using UnityEngine.SceneManagement;

namespace VR_Showroom.Scripts
{
    public class TeleportationRoom : MonoBehaviour
    {
        #region Public Fields

        #endregion


        #region Private Serializable Fields

        #endregion


        #region Private Fields

        #endregion


        #region MonoBehaviour CallBacks

        // Check trigger of teleportation zone and teleport player if enter on it
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player") && SceneManager.GetActiveScene().name != "Hall")
                SceneManager.LoadScene("VR_Showroom/Scenes/Hall");
            else if (other.CompareTag("Player") && SceneManager.GetActiveScene().name != "Art_Gallery")
                SceneManager.LoadScene("VR_Showroom/Scenes/Art_Gallery");
        }

        #endregion


        #region MonoBehaviourPun CallBacks

        #endregion


        #region Public Methods

        #endregion


        #region Private Methods

        #endregion
    }
}