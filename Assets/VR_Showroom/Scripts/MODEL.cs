﻿/*
* Copyright Digital Mate
* Author: YoNeXia <yohann.neraud@gmail.com>
* Projects: https://gitlab.com/YoNeXia or https://github.com/YoNeXia
* Git: https://github.com/alicework/digitalmate-show-room
*/

using UnityEngine;

namespace VR_Showroom.Scripts
{
    public class MODEL : MonoBehaviour
    {
        #region Public Fields

        #endregion


        #region Private Serializable Fields

        #endregion


        #region Private Fields

        #endregion


        #region MonoBehaviour CallBacks

        #endregion


        #region MonoBehaviourPun CallBacks

        #endregion


        #region Public Methods

        #endregion


        #region Private Methods

        #endregion
    }
}