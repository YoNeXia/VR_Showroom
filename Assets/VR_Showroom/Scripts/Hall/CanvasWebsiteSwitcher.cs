﻿/*
* Copyright Digital Mate
* Author: YoNeXia <yohann.neraud@gmail.com>
* Projects: https://gitlab.com/YoNeXia or https://github.com/YoNeXia
* Git: https://github.com/alicework/digitalmate-show-room
*/

using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace VR_Showroom.Scripts.Hall
{
    public class CanvasWebsiteSwitcher : MonoBehaviour
    {
        #region Public Fields

        #endregion


        #region Private Serializable Fields

        [SerializeField] private List<GameObject> _canvas;
        [SerializeField] private Transform _playerTransform;
        [SerializeField] private float _distance;

        #endregion


        #region Private Fields

        private GameObject _canvaUsing, _canvaNeedToBeStop;

        #endregion


        #region MonoBehaviour CallBacks

        private void Start()
        {
            // Initialize canvas variables to first canvas by default
            _canvaNeedToBeStop = _canvaUsing = _canvas[0];
        }

        private void Update()
        {
            CheckClosestCanvas();
        }

        #endregion


        #region MonoBehaviourPun CallBacks

        #endregion


        #region Public Methods

        #endregion


        #region Private Methods

        // Check every frame the closest canvas with web pages to the player
        private void CheckClosestCanvas()
        {
            List<float> distances = new List<float>();

            // Calculate every canvas distances to the player 
            foreach (var canva in _canvas)
            {
                distances.Add(Vector3.Distance(_playerTransform.position,
                    canva.transform.position));
            }

            // Save the smaller distance to canvas need to be used
            _canvaUsing = _canvas[distances.IndexOf(distances.Min())];

            DisplayCanva(_canvaUsing);
            StopCanva(_canvaNeedToBeStop);

            // Change the canvas need to be stopped 
            _canvaNeedToBeStop = _canvaUsing;
        }

        private void DisplayCanva(GameObject canva)
        {
            // Activate closest canvas if the player is near to him
            canva.SetActive(Vector3.Distance(_playerTransform.position,
                                canva.transform
                                    .position) <
                            _distance);
        }

        private void StopCanva(GameObject canva)
        {
            // Disable canvas if the closest change
            if (canva == _canvaUsing) return;

            canva.SetActive(false);
        }

        #endregion
    }
}