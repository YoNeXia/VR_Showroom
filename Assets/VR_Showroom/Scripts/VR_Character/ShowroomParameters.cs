﻿/*
* Copyright Digital Mate
* Author: YoNeXia <yohann.neraud@viacesi.fr>
* Projects: https://gitlab.com/YoNeXia or https://github.com/YoNeXia
* Git: https://github.com/alicework/digitalmate-show-room
*/

using UnityEngine;

namespace VR_Showroom.Scripts.VR_Character
{
    public class ShowroomParameters
    {
        #region Public Fields

        public const string PLAYER_READY = "IsPlayerReady";
        public const string PLAYER_LOADED_LEVEL = "PlayerLoadedLevel";

        #endregion


        #region Private Serializable Fields

        #endregion


        #region Private Fields

        #endregion


        #region MonoBehaviour CallBacks

        #endregion


        #region MonoBehaviourPun CallBacks

        #endregion


        #region Public Methods

        public static Color GetColor(int colorChoice)
        {
            switch (colorChoice)
            {
                case 0: return Color.red;
                case 1: return Color.green;
                case 2: return Color.blue;
                case 3: return Color.yellow;
                case 4: return Color.cyan;
                case 5: return Color.grey;
                case 6: return Color.magenta;
                case 7: return Color.white;
            }

            return Color.black;
        }

        #endregion


        #region Private Methods

        #endregion
    }
}