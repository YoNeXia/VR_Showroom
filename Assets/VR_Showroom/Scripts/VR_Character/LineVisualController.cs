﻿/*
* Copyright Digital Mate
* Author: YoNeXia <yohann.neraud@viacesi.fr>
* Projects: https://gitlab.com/YoNeXia or https://github.com/YoNeXia
* Git: https://github.com/alicework/digitalmate-show-room
*/

using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace VR_Showroom.Scripts.VR_Character
{
    public class LineVisualController : MonoBehaviour
    {
        #region Public Fields

        public XRController lineVisual;
        public InputHelpers.Button activationTeleportButton;

        #endregion


        #region Private Serializable Fields

        #endregion


        #region Private Fields

        private float _activationThreshold = 0.1f;

        #endregion


        #region MonoBehaviour CallBacks

        private void Update()
        {
            if (lineVisual)
            {
                lineVisual.gameObject.SetActive(CheckIfTeleportButtonIsPressed(lineVisual));
            }
        }

        #endregion


        #region MonoBehaviourPun CallBacks

        #endregion


        #region Public Methods

        #endregion


        #region Private Methods

        private bool CheckIfTeleportButtonIsPressed(XRController controller)
        {
            controller.inputDevice.IsPressed(activationTeleportButton, out bool isActivated,
                _activationThreshold);
            return isActivated;
        }

        #endregion
    }
}