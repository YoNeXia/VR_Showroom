﻿/*
* Copyright Digital Mate
* Author: YoNeXia <yohann.neraud@gmail.com>
* Projects: https://gitlab.com/YoNeXia or https://github.com/YoNeXia
* Git: https://github.com/alicework/digitalmate-show-room
*/

using UnityEngine;
using UnityEngine.Video;

namespace VR_Showroom.Scripts.Art_Gallery
{
    public class VideoController : MonoBehaviour
    {
        #region Public Fields

        #endregion


        #region Private Serializable Fields

        [SerializeField] private VideoPlayer _videoPlayer;
        [SerializeField] private Transform _playerTransform;
        [SerializeField] private float _distance;

        #endregion


        #region Private Fields

        #endregion


        #region MonoBehaviour CallBacks

        private void Update()
        {
            // Activate video player when player is near to him
            _videoPlayer.enabled = Vector3.Distance(_playerTransform.position, GetComponent<Transform>().position) <
                                   _distance;
        }

        #endregion


        #region MonoBehaviourPun CallBacks

        #endregion


        #region Public Methods

        #endregion


        #region Private Methods

        #endregion
    }
}