﻿/*
* Copyright Digital Mate
* Author: YoNeXia <yohann.neraud@gmail.com>
* Projects: https://gitlab.com/YoNeXia or https://github.com/YoNeXia
* Git: https://github.com/alicework/digitalmate-show-room
*/

using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Video;

namespace VR_Showroom.Scripts.Hall
{
    public class VideoAndMaterialSwitcher : MonoBehaviour
    {
        #region Public Fields

        #endregion


        #region Private Serializable Fields

        [SerializeField] private Material _offMaterial;
        [SerializeField] private Material _onMaterial;
        [SerializeField] private List<GameObject> _screens;
        [SerializeField] private Transform _playerTransform;
        [SerializeField] private float _distance;

        #endregion


        #region Private Fields

        private GameObject _screenUsing, _screenNeedToBeStop;

        #endregion


        #region MonoBehaviour CallBacks

        private void Start()
        {
            // Initialize screens variables to first screen by default
            _screenNeedToBeStop = _screenUsing = _screens[0];
        }

        private void Update()
        {
            CheckClosestVideo();
        }

        #endregion


        #region MonoBehaviourPun CallBacks

        #endregion


        #region Public Methods

        #endregion


        #region Private Methods

        // Check every frame the closest screens with video to the player
        private void CheckClosestVideo()
        {
            List<float> distances = new List<float>();

            // Calculate every screens distances to the player
            foreach (var screen in _screens)
            {
                distances.Add(Vector3.Distance(_playerTransform.position,
                    screen.transform.position));
            }

            // Save the smaller distance to screen need to be used
            _screenUsing = _screens[distances.IndexOf(distances.Min())];

            PlayVideo(_screenUsing);
            StopVideo(_screenNeedToBeStop);

            // Change the video need to be stopped
            _screenNeedToBeStop = _screenUsing;
        }

        private void PlayVideo(GameObject screen)
        {
            // Activate closest video if the player is near to him
            screen.GetComponent<VideoPlayer>().enabled = Vector3.Distance(_playerTransform.position,
                                                             screen.transform
                                                                 .position) <
                                                         _distance;

            // Change the screen material to see the video
            screen.GetComponent<MeshRenderer>().material =
                screen.GetComponent<VideoPlayer>().enabled ? _onMaterial : _offMaterial;
        }

        private void StopVideo(GameObject screen)
        {
            // Disable video and change material if the closest change
            if (screen == _screenUsing) return;

            screen.GetComponent<MeshRenderer>().material = _offMaterial;
            screen.GetComponent<VideoPlayer>().enabled = false;
        }

        #endregion
    }
}