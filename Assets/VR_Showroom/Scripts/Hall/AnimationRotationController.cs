﻿/*
* Copyright Digital Mate
* Author: YoNeXia <yohann.neraud@gmail.com>
* Projects: https://gitlab.com/YoNeXia or https://github.com/YoNeXia
* Git: https://github.com/alicework/digitalmate-show-room
*/

using System;
using UnityEngine;

namespace VR_Showroom.Scripts.Hall
{
    public class AnimationRotationController : MonoBehaviour
    {
        #region Public Fields

        #endregion


        #region Private Serializable Fields

        [SerializeField] private Transform _playerTransform;
        [SerializeField] private float _distance;
        [SerializeField] private Animator _animatorController;

        #endregion


        #region Private Fields

        #endregion


        #region MonoBehaviour CallBacks

        private void Update()
        {
            // Activate rotation animation when the player is near to the object
            _animatorController.SetBool("playRotation",
                Vector3.Distance(_playerTransform.position, GetComponent<Transform>().position) <
                _distance);
        }

        #endregion


        #region MonoBehaviourPun CallBacks

        #endregion


        #region Public Methods

        #endregion


        #region Private Methods

        #endregion
    }
}