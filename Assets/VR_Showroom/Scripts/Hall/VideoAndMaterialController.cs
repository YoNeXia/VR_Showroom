﻿/*
* Copyright Digital Mate
* Author: YoNeXia <yohann.neraud@gmail.com>
* Projects: https://gitlab.com/YoNeXia or https://github.com/YoNeXia
* Git: https://github.com/alicework/digitalmate-show-room
*/

using System.Collections;
using UnityEngine;
using UnityEngine.Video;

namespace VR_Showroom.Scripts.Hall
{
    public class VideoAndMaterialController : MonoBehaviour
    {
        #region Public Fields

        #endregion


        #region Private Serializable Fields

        [SerializeField] private MeshRenderer _meshRenderer;
        [SerializeField] private Material _offMaterial;
        [SerializeField] private Material _onMaterial;
        [SerializeField] private VideoPlayer _videoPlayer;
        [SerializeField] private Transform _playerTransform;
        [SerializeField] private float _distance;

        #endregion


        #region Private Fields

        #endregion


        #region MonoBehaviour CallBacks

        private void Update()
        {
            // Activate video player when the player is near to him
            _videoPlayer.enabled = Vector3.Distance(_playerTransform.position, GetComponent<Transform>().position) <
                                   _distance;

            // Wait 3s to change the material and avoid to see material without video
            StartCoroutine(WaitBeforeChangeMaterial());
            // Change video material
            _meshRenderer.material = _videoPlayer.enabled ? _onMaterial : _offMaterial;
        }

        #endregion


        #region MonoBehaviourPun CallBacks

        #endregion


        #region Public Methods

        #endregion


        #region Private Methods

        // Timer of 3s
        private IEnumerator WaitBeforeChangeMaterial()
        {
            yield return new WaitForSeconds(3.0f);
        }

        #endregion
    }
}