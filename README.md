# Digital Mate Showroom
**The Digital Mate Showroom is a project to introduce all company knowledges and skills.**

## About
A showroom is a room used to display goods for sale, such as appliances, cars, furniture...
Digital Mate chooses to develop his showroom in VR. VR allows any user to be interacting with the world where he is currently evolving. So visit this showroom will be more fun and entertainment than any other showroom all in presenting knowledges and skills of Digital Mate. 

It include features :
* Oculus Quest VR headset technology used for easy presentations anywhere.
* Multiplayer connection using Photon cloud.
* User interface to control application parameters and player connection.
* 3D environment evolution where you can interact with any modules present in the showroom.

The Digital Mate showroom is made with Unity and using C# scripts to control the player and modules.
The application is mounted in an Oculus Quest VR headset.

## Why use this project?
This project can be used to present all knowledges and skills of Digital Mate. But the project is also made to be an example of what Digital Mate can do for any other customer.
The project is build with modules easily editable. This modules permits to adapt quickly the project for customers, and build a new app working on VR.

## Who can use this project?
The Digital Mate company is the owner of this project. So they can use it to present their knowledges and skills as they want. But they can use it to sell  similar products at any customer wanted a showroom. 

-----------------
## Used technologies
### Oculus Quest headset
Oculus Quest headset is a VR headset of Oculus company. Oculus Quest is our original all-in-one gaming system built for virtual reality. Play almost anywhere with just a VR headset and controllers, without cable connected to a computer. The headset works with an Android system, and you can import applications with the format .apk.

![Oculus Quest headset](https://scontent.fcdg1-1.fna.fbcdn.net/v/t39.2365-6/161127352_285360842974029_7708138752470897604_n.jpg?_nc_cat=111&ccb=1-4&_nc_sid=ad8a9d&_nc_ohc=tS0Hf2zxTRYAX_MJnQA&_nc_ht=scontent.fcdg1-1.fna&oh=6d414f9352f2c5a84b3c6516960d25cb&oe=61172F6F)

### Unity
Unity’s real-time 3D development platform lets artists, designers and developers work together to create amazing immersive and interactive experiences. (Available for Windows, Mac, and Linux.) Unity is using in this project to create virtual application with 3D models. Also using Unity workshop to help the project with models and features.
![Unity Logo](https://unity3d.com/files/images/ogimg.jpg)

-----------------
## Contributors
A project done for Digital Mate.
* **Digital Mate** - _Owner and creator_ - [Website](https://www.digitalmate.fr)
* **Jérôme COUTOU** - _Project initiator_
* **Grégory DAYON** - _Artistic director_
* **Yohann NERAUD** - _Head and developer_ - [GitHub](https://github.com/YoNeXia) and [GitLab](https://gitlab.com/YoNeXia).

-----------------
# Technical details
## Getting started
To use this project on your computer or VR headset, just check releases and find the App to run it or UnityPackage to add it into your Unity project.   
_[Releases Digital Mate showroom page](https://github.com/alicework/digitalmate-show-room/releases)_
