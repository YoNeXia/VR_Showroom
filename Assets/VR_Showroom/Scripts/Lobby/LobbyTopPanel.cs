﻿/*
 * Copyright Digital Mate
 * Author: YoNeXia <yohann.neraud@gmail.com>
 * Projects: https://gitlab.com/YoNeXia or https://github.com/YoNeXia
 * Git: https://github.com/alicework/digitalmate-show-room
*/

using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

namespace VR_Showroom.Scripts.Lobby
{
    public class LobbyTopPanel : MonoBehaviour
    {
        #region Public Fields

        [Header("UI References")]
        public Text ConnectionStatusText;

        #endregion

        
        #region Private Serializable Fields
        #endregion

        
        #region Private Fields

        private readonly string connectionStatusMessage = "    Connection Status: ";

        #endregion

        
        #region MonoBehaviour Callbacks

        public void Update()
        {
            // Update connection status text every frame with connection state
            ConnectionStatusText.text = connectionStatusMessage + PhotonNetwork.NetworkClientState;
        }

        #endregion

        
        #region MonoBehaviourPun Callbacks
        #endregion

        
        #region Public Methods
        #endregion

        
        #region Private Methods
        #endregion
    }
}