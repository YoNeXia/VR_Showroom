﻿/*
* Copyright Digital Mate
* Author: YoNeXia <yohann.neraud@viacesi.fr>
* Projects: https://gitlab.com/YoNeXia or https://github.com/YoNeXia
* Git: https://github.com/alicework/digitalmate-show-room
*/

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

namespace VR_Showroom.Scripts.VR_Character
{
    public class HandPresence : MonoBehaviour
    {
        #region Public Fields

        public bool activeHands;
        public GameObject controllerPrefab;
        public GameObject handPrefab;
        public InputDeviceCharacteristics inputDeviceCharacteristics;

        #endregion


        #region Private Serializable Fields

        #endregion


        #region Private Fields

        private InputDevice _inputDevice;
        private GameObject _spawnedPrefab;
        private Animator _handAnimator;

        #endregion


        #region MonoBehaviour CallBacks

        void Start()
        {
            List<InputDevice> devices = new List<InputDevice>();
            InputDevices.GetDevicesWithCharacteristics(inputDeviceCharacteristics, devices);

            if (devices.Count > 0)
            {
                _inputDevice = devices[0];
                _spawnedPrefab = Instantiate(activeHands ? handPrefab : controllerPrefab, transform);
                _handAnimator = _spawnedPrefab.GetComponent<Animator>();
            }
        }

        void Update()
        {
            UpdateHandAnimation();
        }

        #endregion


        #region MonoBehaviourPun CallBacks

        #endregion


        #region Public Methods

        void UpdateHandAnimation()
        {
            if (_inputDevice.TryGetFeatureValue(CommonUsages.trigger, out float triggerValue))
            {
                _handAnimator.SetFloat("Trigger", triggerValue);
            }
            else
            {
                _handAnimator.SetFloat("Trigger", 0);
            }

            if (_inputDevice.TryGetFeatureValue(CommonUsages.grip, out float gripValue))
            {
                _handAnimator.SetFloat("Grip", gripValue);
            }
            else
            {
                _handAnimator.SetFloat("Grip", 0);
            }
        }

        #endregion


        #region Private Methods

        #endregion
    }
}