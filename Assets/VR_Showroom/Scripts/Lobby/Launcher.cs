﻿/*
 * Copyright Digital Mate
 * Author: YoNeXia <yohann.neraud@gmail.com>
 * Projects: https://gitlab.com/YoNeXia or https://github.com/YoNeXia
 * Git: https://github.com/alicework/digitalmate-show-room
*/

using System.Collections.Generic;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;
using VR_Showroom.Scripts.VR_Character;

namespace VR_Showroom.Scripts.Lobby
{
    public class Launcher : MonoBehaviourPunCallbacks
    {
        #region Public Fields

        #endregion


        #region Private Serializable Fields

        [Header("Login Panel")] [SerializeField]
        GameObject _loginPanel;

        [SerializeField] InputField _playerNameInput;

        [Header("Selection Panel")] [SerializeField]
        GameObject _selectionPanel;

        [Header("Create Room Panel")] [SerializeField]
        GameObject _createRoomPanel;

        [SerializeField] InputField _roomNameInputField;
        [SerializeField] InputField _maxPlayersInputField;

        [Header("Join Random Room Panel")] [SerializeField]
        GameObject _joinRandomRoomPanel;

        [Header("Room List Panel")] [SerializeField]
        GameObject _roomListPanel;

        [SerializeField] GameObject _roomListContent;
        [SerializeField] GameObject _roomListEntryPrefab;

        [Header("Inside Room Panel")] [SerializeField]
        GameObject _insideRoomPanel;

        [SerializeField] Button _startGameButton;
        [SerializeField] GameObject _playerListEntryPrefab;

        #endregion


        #region Private Fields

        private string _gameVersion = "1.0";
        private Dictionary<string, RoomInfo> _cachedRoomList;
        private Dictionary<string, GameObject> _roomListEntries;
        private Dictionary<int, GameObject> _playerListEntries;

        #endregion


        #region MonoBehaviour CallBacks

        void Awake()
        {
            PhotonNetwork.AutomaticallySyncScene = true;

            _cachedRoomList = new Dictionary<string, RoomInfo>();
            _roomListEntries = new Dictionary<string, GameObject>();

            _playerNameInput.text = "Player " + Random.Range(1000, 10000);
            _roomNameInputField.text = "Room " + Random.Range(1000, 10000);
            _maxPlayersInputField.text = "" + Random.Range(3, 10);
        }

        #endregion


        #region MonoBehaviourPun CallBacks

        // Called after the connection to the master is established and authenticated
        public override void OnConnectedToMaster()
        {
            Debug.Log("PUN Basics Tutorial/Launcher: OnConnectedToMaster() was called by PUN");

            SetActivePanel(_selectionPanel.name);
        }

        // Update lobby list when a player join Lobby scene
        public override void OnJoinedLobby()
        {
            Debug.Log("PUN Basics Tutorial/Launcher: OnJoinedLobby() was called by PUN");

            _cachedRoomList.Clear();
            ClearRoomListView();
        }

        // Update lobby list when a player left Lobby scene
        public override void OnLeftLobby()
        {
            Debug.Log("PUN Basics Tutorial/Launcher: OnLeftLobby() was called by PUN");

            _cachedRoomList.Clear();
            ClearRoomListView();
        }

        // Update for local player
        // Update lobby and player when he is entering in a room
        public override void OnJoinedRoom()
        {
            Debug.Log("PUN Basics Tutorial/Launcher: OnJoinedRoom() was called by PUN");

            _cachedRoomList.Clear();
            SetActivePanel(_insideRoomPanel.name);

            // Check value of player list
            if (_playerListEntries == null)
            {
                _playerListEntries = new Dictionary<int, GameObject>();
            }

            // Get players in room from server and add the new player
            foreach (Player p in PhotonNetwork.PlayerList)
            {
                GameObject entry = Instantiate(_playerListEntryPrefab, _insideRoomPanel.transform);
                entry.transform.localScale = Vector3.one;
                entry.GetComponent<PlayerListEntry>().Initialize(p.ActorNumber, p.NickName);

                object isPlayerReady;
                if (p.CustomProperties.TryGetValue(ShowroomParameters.PLAYER_READY, out isPlayerReady))
                {
                    entry.GetComponent<PlayerListEntry>().SetPlayerReady((bool) isPlayerReady);
                }

                _playerListEntries.Add(p.ActorNumber, entry);
            }

            // Activate start button only if player is ready
            _startGameButton.gameObject.SetActive(CheckPlayersReady());

            // Set and send parameters of player for server
            Hashtable props = new Hashtable
            {
                {ShowroomParameters.PLAYER_LOADED_LEVEL, false}
            };
            PhotonNetwork.LocalPlayer.SetCustomProperties(props);
        }

        // Update lobby and pLayer when he is leaving a room
        public override void OnLeftRoom()
        {
            Debug.Log("PUN Basics Tutorial/Launcher: OnLeftRoom() was called by PUN");

            SetActivePanel(_selectionPanel.name);

            // Clear the list
            foreach (GameObject entry in _playerListEntries.Values)
            {
                Destroy(entry.gameObject);
            }

            _playerListEntries.Clear();
            _playerListEntries = null;
        }

        // Update for room on server
        // Check and send parameters of player before and update player list in the room
        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            Debug.Log("PUN Basics Tutorial/Launcher: OnPlayerEnteredRoom() was called by PUN");

            // Instantiate player and his parameters in room
            GameObject entry = Instantiate(_playerListEntryPrefab, _insideRoomPanel.transform);
            entry.transform.localScale = Vector3.one;
            entry.GetComponent<PlayerListEntry>().Initialize(newPlayer.ActorNumber, newPlayer.NickName);

            _playerListEntries.Add(newPlayer.ActorNumber, entry);

            // Activate start button only if player is ready
            _startGameButton.gameObject.SetActive(CheckPlayersReady());
        }

        // Delete player from player list of room
        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            Debug.Log("PUN Basics Tutorial/Launcher: OnPlayerLeftRoom() was called by PUN");

            // Delete player from room player list
            Destroy(_playerListEntries[otherPlayer.ActorNumber].gameObject);
            _playerListEntries.Remove(otherPlayer.ActorNumber);

            // Activate start button only if player is ready
            _startGameButton.gameObject.SetActive(CheckPlayersReady());
        }

        // Update player list from server when any player join or left the room
        public override void OnRoomListUpdate(List<RoomInfo> roomList)
        {
            Debug.Log("PUN Basics Tutorial/Launcher: OnRoomListUpdate() was called by PUN");

            ClearRoomListView();

            UpdateCachedRoomList(roomList);
            UpdateRoomListView();
        }

        // Room creation failed so back to last panel in Lobby scene
        public override void OnCreateRoomFailed(short returnCode, string message)
        {
            Debug.Log("PUN Basics Tutorial/Launcher: OnCreateRoomFailed() was called by PUN");

            SetActivePanel(_selectionPanel.name);
        }

        // Room joined failed so back to last panel in Lobby scene
        public override void OnJoinRoomFailed(short returnCode, string message)
        {
            Debug.Log("PUN Basics Tutorial/Launcher: OnJoinRoomFailed() was called by PUN");

            SetActivePanel(_selectionPanel.name);
        }

        // Random room joined failed so create new one and join it
        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            Debug.Log("PUN Basics Tutorial/Launcher: OnJoinRandomFailed() was called by PUN");

            string roomName = "Room " + Random.Range(1000, 10000);

            RoomOptions options = new RoomOptions {MaxPlayers = 8};

            PhotonNetwork.CreateRoom(roomName, options, null);
        }

        // If player left lobby and other one is waiting to start in same room, change master client to him
        public override void OnMasterClientSwitched(Player newMasterClient)
        {
            Debug.Log("PUN Basics Tutorial/Launcher: OnMasterClientSwitched() was called by PUN");

            if (PhotonNetwork.LocalPlayer.ActorNumber == newMasterClient.ActorNumber)
            {
                // Activate start button only if player is ready
                _startGameButton.gameObject.SetActive(CheckPlayersReady());
            }
        }

        // Update player when his parameters are modified
        public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
        {
            Debug.Log("PUN Basics Tutorial/Launcher: OnPlayerPropertiesUpdate() was called by PUN");

            // Create player list if it empty
            if (_playerListEntries == null)
            {
                _playerListEntries = new Dictionary<int, GameObject>();
            }

            // Add player parameters to player list
            GameObject entry;
            if (_playerListEntries.TryGetValue(targetPlayer.ActorNumber, out entry))
            {
                object isPlayerReady;
                if (changedProps.TryGetValue(ShowroomParameters.PLAYER_READY, out isPlayerReady))
                {
                    entry.GetComponent<PlayerListEntry>().SetPlayerReady((bool) isPlayerReady);
                }
            }

            // Activate start button only if player is ready
            _startGameButton.gameObject.SetActive(CheckPlayersReady());
        }

        #endregion


        #region Public Methods

        // Login button is clicked
        public void OnLoginButtonClicked()
        {
            string playerName = _playerNameInput.text;

            // Check if player name is not empty
            if (!playerName.Equals(""))
            {
                PhotonNetwork.LocalPlayer.NickName = playerName;
                PhotonNetwork.ConnectUsingSettings();
            }
            else
            {
                Debug.LogError("Player Name is invalid.");
            }
        }

        // Create room button is clicked
        public void OnCreateRoomButtonClicked()
        {
            string roomName = _roomNameInputField.text;
            roomName = (roomName.Equals(string.Empty)) ? "Room " + Random.Range(1000, 10000) : roomName;

            byte maxPlayers;
            byte.TryParse(_maxPlayersInputField.text, out maxPlayers);
            maxPlayers = (byte) Mathf.Clamp(maxPlayers, 2, 8);

            RoomOptions options = new RoomOptions {MaxPlayers = maxPlayers, PlayerTtl = 10000};

            PhotonNetwork.CreateRoom(roomName, options, null);
        }

        // Join random room button is clicked
        public void OnJoinRandomRoomButtonClicked()
        {
            SetActivePanel(_joinRandomRoomPanel.name);

            PhotonNetwork.JoinRandomRoom();
        }

        // Room list button is clicked
        public void OnRoomListButtonClicked()
        {
            if (!PhotonNetwork.InLobby)
            {
                PhotonNetwork.JoinLobby();
            }

            SetActivePanel(_roomListPanel.name);
        }

        // Back button is clicked
        public void OnBackButtonClicked()
        {
            if (PhotonNetwork.InLobby)
            {
                PhotonNetwork.LeaveLobby();
            }

            SetActivePanel(_selectionPanel.name);
        }

        // Start button is clicked
        public void OnStartGameButtonClicked()
        {
            PhotonNetwork.CurrentRoom.IsOpen = false;
            PhotonNetwork.CurrentRoom.IsVisible = false;

            PhotonNetwork.LoadLevel("Showroom");
        }

        // Leave button is clicked
        public void OnLeaveGameButtonClicked()
        {
            PhotonNetwork.LeaveRoom();
        }

        // Change active panel in Lobby scene
        public void SetActivePanel(string activePanel)
        {
            _loginPanel.SetActive(activePanel.Equals(_loginPanel.name));
            _selectionPanel.SetActive(activePanel.Equals(_selectionPanel.name));
            _createRoomPanel.SetActive(activePanel.Equals(_createRoomPanel.name));
            _joinRandomRoomPanel.SetActive(activePanel.Equals(_joinRandomRoomPanel.name));
            _roomListPanel.SetActive(activePanel.Equals(_roomListPanel.name));
            _insideRoomPanel.SetActive(activePanel.Equals(_insideRoomPanel.name));
        }

        // Active start button if player properties are updated
        public void LocalPlayerPropertiesUpdated()
        {
            _startGameButton.gameObject.SetActive(CheckPlayersReady());
        }

        #endregion


        #region Private Methods

        // Clear room list view in Lobby scene
        private void ClearRoomListView()
        {
            // Check all elements of list in lobby and clear them 
            foreach (GameObject entry in _roomListEntries.Values)
            {
                Destroy(entry.gameObject);
            }

            _roomListEntries.Clear();
        }

        // Check if player is ready to enter in room
        private bool CheckPlayersReady()
        {
            // Check if player is master client
            if (!PhotonNetwork.IsMasterClient)
            {
                return false;
            }

            // Check if all players in player list are ready
            foreach (Player p in PhotonNetwork.PlayerList)
            {
                object isPlayerReady;
                if (p.CustomProperties.TryGetValue(ShowroomParameters.PLAYER_READY, out isPlayerReady))
                {
                    if (!(bool) isPlayerReady)
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        // Update room list available
        private void UpdateCachedRoomList(List<RoomInfo> roomList)
        {
            foreach (RoomInfo info in roomList)
            {
                // Remove room from cached room list if it got closed, became invisible or was marked as removed
                if (!info.IsOpen || !info.IsVisible || info.RemovedFromList)
                {
                    if (_cachedRoomList.ContainsKey(info.Name))
                    {
                        _cachedRoomList.Remove(info.Name);
                    }

                    continue;
                }

                // Update cached room info
                if (_cachedRoomList.ContainsKey(info.Name))
                {
                    _cachedRoomList[info.Name] = info;
                }
                // Add new room info to cache
                else
                {
                    _cachedRoomList.Add(info.Name, info);
                }
            }
        }

        // Update room list available in lobby scene view
        private void UpdateRoomListView()
        {
            // 
            foreach (RoomInfo info in _cachedRoomList.Values)
            {
                GameObject entry = Instantiate(_roomListEntryPrefab, _roomListContent.transform);
                entry.transform.localScale = Vector3.one;
                entry.GetComponent<RoomListEntry>().Initialize(info.Name, (byte) info.PlayerCount, info.MaxPlayers);

                _roomListEntries.Add(info.Name, entry);
            }
        }

        #endregion
    }
}